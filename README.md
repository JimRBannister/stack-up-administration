# Stack Up Administration

- [Getting started](#getting-started)
    - [.env file](#env-file)
- [References](#handy-references)


## Activities

- [Deployments and configurations](##deployments-and-configurations)
- [Securely store passwords used by the Server](#securely-store-passwords-used-by-the-server)
- [Test our Server with Jest](#test-our-server-with-jest)
- [Develop a React app with ChartJS in no time](#develop-a-react-app-with-chartjs-in-no-time)

## Getting started

Please start by reading the [Stack Up Courseware](https://jimrbannister.gitlab.io/stack-up/#top).

The *Mock the Server* activity is not covered here.


To use docker stack first initialise docker swarm if you have not already done so.

```bash
docker swarm init
```

## Deployments and configurations

Fork a new project from https://gitlab.com/JimRBannister/stack-up-administration

Unlink the fork see your new project settings -> General -> Advanced in GitLab.

Create a stackup projects directory if one doesn't exist. All projects will be held under this sub-directory.

```bash
mkdir stackup
```

Change directory into your project directory.
```bash
cd stackup
```


Clone your new repository using git

```bash
git clone <url>
```


Change directory into the new directory created by the clone

```bash
cd <repo name>
```


Checkout the development branch
```bash
git checkout dev
```

Change directory in the first deployments script directory

```bash
cd deployment/begin
```

docker-compose-deployment-start.yaml makes use of existing images that are available Docker Hub.


This first exercise deploys a Postgres timeline Service and a Hasura GraphQL Service. The Hasura GraphQL Service can be accessed via after it has been started via http://localhost:9001/console/

View the docker compose file found in sub-directory. It contains the specification for two services:

**timescale** This is our Postgres database with Timescale extension installed. It is configured to run at port 5432.

**graphql-engine** This is a Hasura GraphQL Engine instance, i.e. a GraphQL server which allows access to the database and provides GraphQL APIs to acccess date.
It is configured to run at the port 8080, and this port is mapped to port 9001 of the machine that this docker container runs on.
This means that you can access this GraphQL server through at localhost:9001 from the machine that provides the service. If the service is started on your local machine the use this address.

This demonstrates the use of port binding as part of Twelve Factor Apps. Each process has its communication protocols bound to a usually non-standard port, allowing it run in a container in an isolated fashion.
**timescale** is a Postgres service, and its port is set by default to 5432. **graphql-engine** is using port 8080. This can be mapped in Docker Compose to different port that is exposed and made available outside Docker.
It may not always be desirable to expose all services such as database services to external access.

This docker compose configuration instructs the Docker engine to obtain two images timescaledb and graphql-engine. Both are prebuilt and are held in the docker hub repository. Please see:

[postgres timescale image](https://hub.docker.com/r/timescale/timescaledb)

[hasura graphql engine image](https://hub.docker.com/r/hasura/graphql-engine)

Docker encourages a neat and clear separation between builds, release and running of applications, see Factor V of Twelve Factor Apps. Build, release, run!
The timescaledb image represents the Postgres/Timeline database service, which will become available when deployed (when the stack is started, see the command below). The database will be permanently stored in the directory indicated by the volume directory. If a volume is not defined the database is only kept for the time the service is running and is lost when the service is stopped.  A database service without a volume can be useful for test database containers when a database needs a clean start and data can be removed when the service finishes.
The timeline password and graphql-engine password are provided in this configuration file. This is far from ideal and is **not recommended**. We will see how passwords and other sensitive data can be held securely next.

The graqhql middleware is represented by the graphql-engine. This is software provided by hasura. This will help us to get something deployed quickly.
The hasura engine can be replaced by your own GraphQL service instead, for example, by making use of Apollo GQL.
External access can be been explicitly be provided to the database via port mapping - see the _ports_ directive.
The timeline service uses  port 5432, and it can't connect through to this database when we start it up next with the docker stack unless the _ports_ directive is declared.
graphql engine is listening on port 8080 and this is mapped to 9001 to allow external use.

timescaledb implicitly exposes the port 5432 so that the graphql engine can connect it to within the Docker.
The graphql-engine depends on the timescale so will wait for the timeline service to start first. The command declaration is used to define the graphql-engine to start.
The last part of the configuration concerns the volume. This defines the storage volume docker uses and this can be shared amongst services if necessary.
In this case the timeline database is held in the volume and this data will be persisted and won't be lost between service restarts.


```bash
docker stack deploy --compose-file docker-compose-deployment-start.yaml --with-registry-auth hartreedemo 
```

Check you can log-in to the service before proceeding via your web browser using the following address

```bash
http://localhost:9001/console
```

Before a new deployment stack is started remove the old one first:
```bash
docker stack rm hartreedemo
```

From the project top-level directory
```bash
cd deployment/end
```

Our next step is to securely store secrets in Docker. For further information see [Manage sensitive data with Docker secrets](https://docs.docker.com/engine/swarm/secrets/)
There are alternatives to holding secrets in Docker, which include, for example, Vault by Hashicorp, and service by Cloud Providers, such as AWS Secrets Manager. These alternatives will not be covered here.

Create secrets for the postgress password and the harura admin secret as follows.

```bash
echo "postgrespassword" | docker secret create postgres_password - 
```

To see the list of secrets, but not the values
```bash
docker secret ls 
```

Now view the docker compose file in the current directory and note the use of the docker secrets and the removal of the postgres password against the postgres service.

There is an issue with using secrets with the hasura service because it expects the HASURA_GRAPHQL_DATABASE_URL to be set. We will introduce our own GraphQL service as a
replacement to the Hasura GraphQL engine to overcome this problem.


```bash
docker stack deploy --compose-file docker-compose-deployment-end.yaml --with-registry-auth  hartreedemo
```

Now access the harura engine service via the web page and create the tables and view required by the rest of the course.

Select the top-menu item `DATA `and then `SQL` from the side-bar !

`CREATE SCHEMA hartree;`

the check boxes should be left unselected and then press` Run.`

You will get a reassuring message in green to say that the command has been successful if all goes well.

Now again the SQL window :-

```
CREATE TABLE hartree.temperature (
  temperature numeric not null,
  location text not null,
  recordedat timestamptz not null default now()
);
```

`Select` the `track this` and `cascade metadata` check boxes and press `Run`

```
SELECT create_hypertable('hartree.temperature', 'recordedat');
```

`Select` the `track this` and `cascade metadata` check boxes and press `Run`

```
CREATE VIEW hartree.lastTwentyMinTemperatures AS (
  SELECT time_bucket('5 seconds', recordedat) AS fivesecinterval,
  location,
    MAX(temperature) AS maxtemp
  FROM hartree.temperature
  WHERE recordedat > NOW() - interval '20 minutes'
  GROUP BY fivesecinterval, location
  ORDER BY fivesecinterval ASC
);   
```

`Select` the `track this` and `cascade metadata` check boxes and press `Run`

_**We have now setup the database with timescale!! Happy days! **_

Test that the service works as expected before moving on

`select` the "API" tab from the top-menu of HASURA and enter the following in the text entry box below the play/run symbol. This will insert an entry into the temperature table.

```
mutation {
 insert_hartree_temperature (
  objects: [{
    temperature: 24.4
    location: "Manchester"
   }]
 ) {
    returning {
      recordedat
      temperature
    }
  }
}
```

press the `Run symbol` to execute this GraphQL mutation

To view the entries entered into temperature table via graphQL:

```
query {
  hartree_temperature {
    recordedat
    temperature
    location
  }
}
```

press the `Run symbol` to execute the above GraphQL query

That's the end of the activity, folks. 

The database tables and views created here will be used in subsequent Activities of part of this course.



## Securely store passwords used by the Server

Create a graphql server image first by referring to the stack-up-graphql README.md

From the stack-up-adminstration top level directory change directory as follows.

```bash
cd secure/begin
``` 


Deploy the database service and graphl service

```bash
docker stack deploy --compose-file docker-compose-secure-start.yaml --with-registry-auth  hartreedemo
```

View the container images that are now running as part of the deployment.

```bash
docker container ls
```

Find the hartree-gql container image instance and copy the Container Id of that instance, and use the docker logs command as below to 
show information logged by the running hartree-gql container.

```bash
docker logs <CONTAINER_ID>
```

You will find that the "Secret unobtainable" error has occurred. The GraphQL Server will not be able to retrieve data from the database.

Now remove the hartreedemo stack before moving on.

```bash
docker stack rm hartreedemo
```

Now change to the directory where a secure docker-compose yaml file has been provided.

```bash
cd ../end
```

Compare the two docker-compose files; the one in the begin directory and this directory. Here we are making use of the secret created in the previous activity.

```bash
docker stack deploy --compose-file docker-compose-secure-end.yaml --with-registry-auth  hartreedemo
```

Now check the log for hartree-graphql as previously shown. There should not be an error and the data can now be retrieved.

Add a temperature and then retrieve the data just added via curl. 

```bash
curl 'http://localhost:9002/graphql'   -X POST   -H 'content-type: application/json'  --data '{    "query":"mutation { addTemperature(location: \"London\", temperature:18.0) }"  }'
```

```bash
curl 'http://localhost:9002/graphql' -X POST -H 'content-type: application/json' --data '{"query":"query { recentTemperatures { fivesecinterval, location,maxtemp }  }"}'
```

Now moving on to look at concurrency and disposability of the GraphQL Server.

```bash
cd ../../nginx
```

Create a load balancing image which makes use of nginx. nginx will be used to load balance across multiple GraphQL Server (hartree-graphql) instances.

```bash
docker build -f Dockerfile -t stackup/hartree-lb .
```

Let's now see the load balancing in action...!

```bash
cd ../availability
```

View the file docker-compose-nginx-gql.yaml. The networks declaration allows the graphql-engine to communicate with timescale.
Also, see deploy declaration and the replicas.

Deploy the load balancer configuration

```bash
docker stack deploy --compose-file docker-compose-nginx-gql.yaml --with-registry-auth  hartreedemo
```

Check the number of hartree-gql instances started

```bash
docker container ls
```

Repeat the following command six times. Note the use of the 8181 port number and how this relates to the docker-compose configuration.
```bash
curl 'http://localhost:8181/graphql'   -X POST   -H 'content-type: application/json'  --data '{    "query":"mutation { addTemperature(location: \"London\", temperature:18.0) }"  }'
```

Now view the containers logs via the docker logs command to see how the load balancing has performed against the hartree-gql instances.

```bash
docker logs <CONTAINER_ID>
```

Clear up before we move to the next activity.

```bash
docker stack rm hartreedemo
```


## Test our Server with Jest

Refer to the stack-up-graphql project to create the test image first.

From the stack-up-adminstration top level directory change directory as follows.

```bash
cd test
``` 


Test the GraphQL Server against the database. This test service makes use of database scripts to create the tables and views required before the database service starts.
See the scripts sub-directory.


```bash
docker-compose up  --abort-on-container-exit --exit-code-from graphql-engine

```

See the output from the docker-compose; all the tests results and a test coverage report are provided.

The password is exposed in the docker-compose file in this case. Consider creating a docker secret for the POSTGRES_PASSWORD during testing particularly if sensitive data is used in testing.


## Develop a React app with ChartJS in no time

Refer to the stack-up-react project to create the react image first.

From the stack-up-adminstration top level directory change directory as follows.

```bash
cd react
``` 

Deploy the full stack, which includes the database, graphql server, and react services as defined in the docker compose file.

```bash
docker stack deploy --compose-file docker-compose-fullstack.yaml --with-registry-auth  hartreedemo
```



View the web browser using the web address:
```
http://localhost:3000/
```

Repeat the following command six or more times - once every few seconds. Note the use of the 8181 port number and how this relates to the docker-compose configuration.

```bash
curl 'http://localhost:8181/graphql'   -X POST   -H 'content-type: application/json'  --data '{    "query":"mutation { addTemperature(location: \"London\", temperature:18.0) }"  }'
```

You will notice that the graph is updated every 5 seconds.

Thanks for your undertaking the Stack Up course.

## Handy References!


- [Docker](https://www.docker.com/)
- [Docker compose](https://docs.docker.com/compose/)
